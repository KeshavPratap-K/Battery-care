import 'package:batteryinfo/models_providers/theme_provider.dart';

var deviceDetailsDataListVar = [];
var deviceDetailsTitleListVar = [];
var deviceDetailsIconListVar = [];
int deviceDetailsListVarLength = 0;

double screenHeight = 0;
double screenWidth = 0;
//ThemeProvider themeProvider;

class BatteryData {
  final int time;
  final int level;

  BatteryData(this.time, this.level);
}
