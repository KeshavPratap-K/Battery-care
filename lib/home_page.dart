import 'dart:async';

import 'package:battery_plus/battery_plus.dart';
import 'package:batteryinfo/device_details.dart';
import 'package:batteryinfo/globals/global_var.dart';
import 'package:flutter/material.dart';
import 'package:batteryinfo/components/z_animated_toggle.dart';
import 'package:batteryinfo/models_providers/theme_provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var battery = Battery();
  int percentage = 0;
  late Timer timer;
  BatteryState batteryState = BatteryState.full;
  String batteryStatus = "Discharging";
  Color batterycolor = const Color.fromARGB(255, 2, 241, 14);
  late StreamSubscription streamSubscription;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800));
    super.initState();

    getBatteryPercentage();
    getBatteryState();
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      getBatteryPercentage();
      getBatteryState();
    });
  }

  void getBatteryPercentage() async {
    final level = await battery.batteryLevel;
    percentage = level;

    setState(() {});
  }

  void getBatteryState() async {
    streamSubscription = battery.onBatteryStateChanged.listen((state) {
      batteryState = state;
      switch (batteryState) {
        case BatteryState.full:
          batteryStatus = "Battery Full";
          batterycolor = const Color.fromARGB(255, 2, 241, 14);
          break;
        case BatteryState.charging:
          batteryStatus = "charging";
          batterycolor = const Color.fromARGB(255, 255, 115, 0);
          break;
        case BatteryState.discharging:
          batteryStatus = "discharging";
          batterycolor = const Color.fromARGB(255, 2, 241, 14);
          break;
        case BatteryState.unknown:
          batteryStatus = "unknown";
          batterycolor = const Color.fromARGB(255, 97, 97, 97);
          break;
        default:
          batteryStatus = "something broke";
          batterycolor = const Color.fromARGB(255, 170, 170, 170);
      }

      setState(() {});
    });
  }

  // function to toggle circle animation
  changeThemeMode(bool theme) {
    if (!theme) {
      _animationController.forward(from: 0.0);
    } else {
      _animationController.reverse(from: 1.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    // Now we have access to the theme properties
    final themeProvider = Provider.of<ThemeProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: screenHeight * 0.1),
          child: Column(
            children: [
              ZAnimatedToggle(
                values: const ['Light', 'Dark'],
                onToggleCallback: (v) async {
                  await themeProvider.toggleThemeData();
                  setState(() {});
                  changeThemeMode(themeProvider.isLightTheme);
                },
              ),
              SizedBox(height: screenHeight * 0.1),
              CircularPercentIndicator(
                animation: true,
                animationDuration: 1000,
                radius: screenWidth * 0.35,
                lineWidth: screenWidth * 0.07,
                percent: percentage.toDouble() / 100,
                center: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '$percentage%',
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 30.0),
                    ),
                    Text(
                      batteryStatus,
                      style: const TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 20.0),
                    ),
                  ],
                ),
                progressColor: batterycolor,
                backgroundColor: const Color.fromARGB(255, 97, 97, 97),
                circularStrokeCap: CircularStrokeCap.round,
              ),
              SizedBox(height: screenHeight * 0.1),
              TextButton(
                  style: TextButton.styleFrom(
                    textStyle: const TextStyle(fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        PageTransition(
                            type: PageTransitionType.rightToLeft,
                            child: const DeviceDetails()));
                  },
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: const [
                      Text("More Details ", style: TextStyle(fontSize: 20)),
                      Icon(Icons.arrow_forward_ios, size: 20),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

// #time for finishing touches! I
