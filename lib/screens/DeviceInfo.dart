import 'dart:io';

import 'package:auto_animated/auto_animated.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../utils.dart';
import 'package:flutter/material.dart';
import 'package:batteryinfo/globals/global_var.dart';

class DeviceInfoList extends StatelessWidget {
  const DeviceInfoList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: Column(
            children: const <Widget>[
              Expanded(
                child: DeviceInfo(),
              ),
            ],
          ),
        ),
      );
}

class DeviceInfo extends StatefulWidget {
  const DeviceInfo({Key? key}) : super(key: key);

  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  @override
  State<DeviceInfo> createState() => _DeviceInfoState();
}

class _DeviceInfoState extends State<DeviceInfo> {
  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    var deviceData = <String, dynamic>{};
    var deviceDataIcons = <String, dynamic>{};

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(
            await DeviceInfo.deviceInfoPlugin.androidInfo);
        deviceDetailsDataListVar = deviceData.keys
            .map((String property) => deviceData[property])
            .toList();
        deviceDetailsTitleListVar =
            deviceData.keys.map((String property) => property).toList();

        deviceDataIcons = deviceDetailsIconListData();
        deviceDetailsIconListVar = deviceDataIcons.keys
            .map((String property) => deviceDataIcons[property])
            .toList();

        deviceDetailsListVarLength = deviceDetailsDataListVar.length;
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    setState(() {});
  }

  Map<String, dynamic> deviceDetailsIconListData() {
    return <String, dynamic>{
      'manufacturer': const FaIcon(FontAwesomeIcons.mobileAlt),
      'model': const FaIcon(FontAwesomeIcons.android),
      'product': const FaIcon(FontAwesomeIcons.microchip),
      'version.securityPatch': const FaIcon(FontAwesomeIcons.codeBranch),
      'version.sdkInt': const FaIcon(FontAwesomeIcons.code),
      'version.release': const FaIcon(FontAwesomeIcons.robot),
      'version.previewSdkInt': const FaIcon(FontAwesomeIcons.robot),
      'version.incremental': const FaIcon(FontAwesomeIcons.robot),
      'version.codename': const FaIcon(FontAwesomeIcons.robot),
      'version.baseOS': const FaIcon(FontAwesomeIcons.robot),
      'board': const FaIcon(FontAwesomeIcons.robot),
      'bootloader': const FaIcon(FontAwesomeIcons.robot),
      'brand': const FaIcon(FontAwesomeIcons.robot),
      'device': const FaIcon(FontAwesomeIcons.robot),
      'display': const FaIcon(FontAwesomeIcons.robot),
      'fingerprint': const FaIcon(FontAwesomeIcons.robot),
      'hardware': const FaIcon(FontAwesomeIcons.robot),
      'host': const FaIcon(FontAwesomeIcons.robot),
      'id': const FaIcon(FontAwesomeIcons.robot),
      'supported32BitAbis': const FaIcon(FontAwesomeIcons.robot),
      'supported64BitAbis': const FaIcon(FontAwesomeIcons.robot),
      'supportedAbis': const FaIcon(FontAwesomeIcons.robot),
      'tags': const FaIcon(FontAwesomeIcons.robot),
      'type': const FaIcon(FontAwesomeIcons.robot),
      'isPhysicalDevice': const FaIcon(FontAwesomeIcons.robot),
      'androidId': const FaIcon(FontAwesomeIcons.robot),
      'systemFeatures': const FaIcon(FontAwesomeIcons.robot),
    };
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  @override
  Widget build(BuildContext context) => LiveList(
        showItemInterval: const Duration(milliseconds: 200),
        showItemDuration: const Duration(milliseconds: 90),
        padding: const EdgeInsets.all(16),
        reAnimateOnVisibility: false,
        scrollDirection: Axis.vertical,
        itemCount: deviceDetailsListVarLength,
        itemBuilder: animationItemBuilder(
          (index) => ListTile(
            leading: deviceDetailsIconListVar[index],
            title: Text(deviceDetailsTitleListVar[index].toString() +
                ":  " +
                deviceDetailsDataListVar[index].toString()),
          ),
          padding: const EdgeInsets.symmetric(vertical: 1),
        ),
      );
}
