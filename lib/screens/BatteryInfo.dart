import 'package:auto_animated/auto_animated.dart';
import 'package:hive/hive.dart';
import '../utils.dart';
import 'package:flutter/material.dart';
import 'package:batteryinfo/globals/global_var.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:path_provider/path_provider.dart';

class LineChart extends StatelessWidget {
  // Defining the data
  final data = [
    BatteryData(0, 1500000),
    BatteryData(1, 1735000),
    BatteryData(2, 1678000),
    BatteryData(3, 1890000),
    BatteryData(4, 1907000),
    BatteryData(5, 2300000),
    BatteryData(6, 2360000),
    BatteryData(7, 1980000),
    BatteryData(8, 2654000),
    BatteryData(9, 2789070),
    BatteryData(10, 3020000),
    BatteryData(11, 3245900),
    BatteryData(12, 4098500),
    BatteryData(13, 4500000),
    BatteryData(14, 4456500),
    BatteryData(15, 3900500),
    BatteryData(16, 5123400),
    BatteryData(17, 5589000),
    BatteryData(18, 5940000),
    BatteryData(19, 6367000),
  ];

  final bool isLightTheme = false;
  var axis = charts.NumericAxisSpec(
      renderSpec: charts.GridlineRendererSpec(
          labelStyle: const charts.TextStyleSpec(
              fontSize: 10, color: charts.MaterialPalette.white),
          lineStyle: charts.LineStyleSpec(
              thickness: 0, color: charts.MaterialPalette.gray.shadeDefault)));

  LineChart({Key? key}) : super(key: key);

  _getSeriesData() {
    List<charts.Series<BatteryData, int>> series = [
      charts.Series(
          id: "BatteryInfo",
          data: data,
          domainFn: (BatteryData series, _) => series.time,
          measureFn: (BatteryData series, _) => series.level,
          colorFn: (BatteryData series, _) =>
              charts.MaterialPalette.blue.shadeDefault)
    ];
    return series;
  }

  Future<bool> getData() async {
    WidgetsFlutterBinding.ensureInitialized();
    final appDocumentDirectory = await getApplicationDocumentsDirectory();

    Hive.init(appDocumentDirectory.path);

    final settings = await Hive.openBox('settings');
    bool themedata = settings.get('isLightTheme') ?? false;
    return themedata;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: FutureBuilder(
          builder: (ctx, snapshot) {
            // Checking if future is resolved or not
            if (snapshot.connectionState == ConnectionState.done) {
              // If we got an error
              if (snapshot.hasError) {
                return const Center(
                  child: Text(
                    'Error occured',
                    style: TextStyle(fontSize: 18),
                  ),
                );

                // if we got our data
              } else if (snapshot.hasData) {
                final themedata = snapshot.data as bool;
                return Scaffold(
                    body: Center(
                  child: Container(
                    height: 550,
                    padding: const EdgeInsets.all(10),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            const Text(
                              "Battery Graph",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Expanded(
                              child: charts.LineChart(
                                _getSeriesData(),
                                animate: true,
                                primaryMeasureAxis: themedata ? null : axis,
                                domainAxis: themedata ? null : axis,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ));
              }
            }

            // Displaying LoadingSpinner to indicate waiting state
            return const Center(
              child: CircularProgressIndicator(),
            );
          },

          // Future that needs to be resolved
          // inorder to display something on the Canvas
          future: getData(),
        ),
      ),
    );
  }

  /* @override
  Widget build(BuildContext context) {
    getCurrentTheme();
    return Scaffold(
        body: Center(
      child: Container(
        height: 550,
        padding: const EdgeInsets.all(10),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                const Text(
                  "Battery Graph",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: charts.LineChart(
                    _getSeriesData(),
                    animate: true,
                    primaryMeasureAxis: !isLightTheme ? axis : null,
                    domainAxis: !isLightTheme ? axis : null,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    ));
  } */

}

class BatteryInfoList extends StatelessWidget {
  const BatteryInfoList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          // Wrapper before Scroll view!
          child: AnimateIfVisibleWrapper(
            showItemInterval: const Duration(milliseconds: 150),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  for (int i = 0; i < 20; i++)
                    AnimateIfVisible(
                      key: Key('$i'),
                      builder: animationBuilder(
                        SizedBox(
                          width: double.infinity,
                          height: 60,
                          child: HorizontalItem(
                            title: '$i',
                          ),
                        ),
                        xOffset: 0.15,
                        padding: const EdgeInsets.all(16),
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      );
}
